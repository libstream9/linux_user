#ifndef STREAM9_LINUX_NAMESPACE_HPP
#define STREAM9_LINUX_NAMESPACE_HPP

namespace stream9::strings {}
namespace stream9::json {}

namespace stream9::linux {

namespace lx { using namespace stream9::linux; }
namespace str { using namespace stream9::strings; }
namespace json { using namespace stream9::json; }

} // namespace stream9::linux

#endif // STREAM9_LINUX_NAMESPACE_HPP
