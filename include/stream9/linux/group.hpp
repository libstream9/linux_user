#ifndef STREAM9_LINUX_USER_GROUP_HPP
#define STREAM9_LINUX_USER_GROUP_HPP

#include "namespace.hpp"

#include <cstddef>
#include <optional>
#include <span>

#include <sys/types.h>
#include <grp.h>

#include <stream9/cstring_ptr.hpp>

namespace stream9::linux {

/*
 * non-reentrant variant
 */
struct ::group* getgrnam(str::cstring_ptr name);
struct ::group* getgrgid(::gid_t);

/*
 * reentrant variant
 */
std::optional<struct ::group>
getgrnam(cstring_ptr name, std::span<char>);

std::optional<struct ::group>
getgrgid(::gid_t, std::span<char>);

/*
 * @return sysconf(_SC_GETPW_R_SIZE_MAX) or 16384 if sysconf return -1
 */
std::size_t getgr_buffer_size() noexcept;

} // namespace stream9::linux

namespace stream9::json {

class value_from_tag;
class value;

void tag_invoke(value_from_tag, json::value&, struct ::group const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_USER_GROUP_HPP
