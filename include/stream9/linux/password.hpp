#ifndef STREAM9_LINUX_USER_PASSWORD_HPP
#define STREAM9_LINUX_USER_PASSWORD_HPP

#include "namespace.hpp"

#include <cstddef>
#include <optional>
#include <span>

#include <sys/types.h>
#include <pwd.h>

#include <stream9/cstring_ptr.hpp>

namespace stream9::linux {

/*
 * non-reentrant variant
 */
struct ::passwd* getpwnam(str::cstring_ptr name);
struct ::passwd* getpwuid(::uid_t);

/*
 * reentrant variant
 */
std::optional<struct ::passwd>
getpwnam(cstring_ptr name, std::span<char>);

std::optional<struct ::passwd>
getpwuid(::uid_t, std::span<char>);

/*
 * @return sysconf(_SC_GETPW_R_SIZE_MAX) or 16384 if sysconf return -1
 */
std::size_t getpw_buffer_size() noexcept;

} // namespace stream9::linux

namespace stream9::json {

class value_from_tag;
class value;

void tag_invoke(value_from_tag, json::value&, struct ::passwd const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_USER_PASSWORD_HPP
