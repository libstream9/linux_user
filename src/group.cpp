#include <stream9/linux/group.hpp>

#include <optional>
#include <span>

#include <grp.h>
#include <sys/types.h>
#include <unistd.h>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

struct ::group*
getgrnam(str::cstring_ptr const name)
{
    errno = 0;
    auto* const rv = ::getgrnam(name);
    if (errno) {
        throw error {
            "getgrnam()",
            lx::make_error_code(errno), {
                { "name", name },
            }
        };
    }

    return rv;
}

struct ::group*
getgrgid(::uid_t const uid)
{
    errno = 0;
    auto* const rv = ::getgrgid(uid);
    if (errno) {
        throw error {
            "getgrgid()",
            lx::make_error_code(errno), {
                { "uid", uid },
            }
        };
    }

    return rv;
}

std::optional<struct ::group>
getgrnam(cstring_ptr name, std::span<char> const buf)
{
    std::optional<struct ::group> result;
    struct ::group* ptr = nullptr;

    result.emplace();
    auto const rc = ::getgrnam_r(
        name,
        &*result,
        buf.data(), buf.size(),
        &ptr
    );
    if (rc == 0) {
        if (ptr == nullptr) {
            result = std::nullopt;
        }
    }
    else {
        throw error {
            "getgrnam_r()",
            lx::make_error_code(rc), {
                { "name", name }
            }
        };
    }

    return result;
}

std::optional<struct ::group>
getgrgid(::uid_t const uid, std::span<char> const buf)
{
    std::optional<struct ::group> result;
    struct ::group* ptr = nullptr;

    result.emplace();
    auto const rc = ::getgrgid_r(
        uid,
        &*result,
        buf.data(), buf.size(),
        &ptr
    );
    if (rc == 0) {
        if (ptr == nullptr) {
            result = std::nullopt;
        }
    }
    else {
        throw error {
            "getgrgid_r()",
            lx::make_error_code(rc), {
                { "uid", uid }
            }
        };
    }

    return result;
}

std::size_t
getgr_buffer_size() noexcept
{
    auto const rv = ::sysconf(_SC_GETGR_R_SIZE_MAX);
    return rv == -1 ? 16384 : static_cast<std::size_t>(rv);
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value& v, struct ::group const& gr)
{
    try {
        auto& obj = v.emplace_object();

        obj["name"] = gr.gr_name;
        obj["passwd"] = gr.gr_passwd;
        obj["gid"] = gr.gr_gid;
        //TODO mem
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::json
