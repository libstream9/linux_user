#include <stream9/linux/password.hpp>

#include <optional>
#include <span>

#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

struct ::passwd*
getpwnam(str::cstring_ptr const name)
{
    errno = 0;
    auto* const rv = ::getpwnam(name);
    if (errno) {
        throw error {
            "getpwnam()",
            lx::make_error_code(errno), {
                { "name", name },
            }
        };
    }

    return rv;
}

struct ::passwd*
getpwuid(::uid_t const uid)
{
    errno = 0;
    auto* const rv = ::getpwuid(uid);
    if (errno) {
        throw error {
            "getpwuid()",
            lx::make_error_code(errno), {
                { "uid", uid },
            }
        };
    }

    return rv;
}

std::optional<struct ::passwd>
getpwnam(cstring_ptr name, std::span<char> const buf)
{
    std::optional<struct ::passwd> result;
    struct ::passwd* ptr = nullptr;

    result.emplace();
    auto const rc = ::getpwnam_r(
        name,
        &*result,
        buf.data(), buf.size(),
        &ptr
    );
    if (rc == 0) {
        if (ptr == nullptr) {
            result = std::nullopt;
        }
    }
    else {
        throw error {
            "getpwnam_r()",
            lx::make_error_code(rc), {
                { "name", name }
            }
        };
    }

    return result;
}

std::optional<struct ::passwd>
getpwuid(::uid_t const uid, std::span<char> const buf)
{
    std::optional<struct ::passwd> result;
    struct ::passwd* ptr = nullptr;

    result.emplace();
    auto const rc = ::getpwuid_r(
        uid,
        &*result,
        buf.data(), buf.size(),
        &ptr
    );
    if (rc == 0) {
        if (ptr == nullptr) {
            result = std::nullopt;
        }
    }
    else {
        throw error {
            "getpwuid_r()",
            lx::make_error_code(rc), {
                { "uid", uid }
            }
        };
    }

    return result;
}

std::size_t
getpw_buffer_size() noexcept
{
    auto const rv = ::sysconf(_SC_GETPW_R_SIZE_MAX);
    return rv == -1 ? 16384 : static_cast<std::size_t>(rv);
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(json::value_from_tag, json::value& v, struct ::passwd const& pw)
{
    try {
        auto& obj = v.emplace_object();

        obj["name"] = pw.pw_name;
        obj["passwd"] = pw.pw_passwd;
        obj["uid"] = pw.pw_uid;
        obj["gid"] = pw.pw_gid;
        obj["gecos"] = pw.pw_gecos;
        obj["dir"] = pw.pw_dir;
        obj["shell"] = pw.pw_shell;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::json
